### Download Websites:
- [ ] <img src="/img_ico/folder.png"> ###
  - [ ] <img src="/img_ico/rlslog.net.png"> [Releaselog | RLSLOG.net](http://www.rlslog.net/)
  - [ ] <img src="/img_ico/ebooks-it.org.png"> [IT eBooks Download Free | eBooks-IT.org](https://ebooks-it.org/)
  - [ ] <img src="/img_ico/1stwebdesigner.com.png"> [The 30 Best Websites for Downloading Free eBooks - 1stWebDesigner](https://1stwebdesigner.com/ebook-download-sites/)
  - [ ] <img src="/img_ico/torrentbutler.eu.png"> [TorrentButler.eu](https://torrentbutler.eu/)
  - [ ] <img src="/img_ico/ebookee.org.png"> [Ebookee: Free Download eBooks Search Engine!](https://ebookee.org/)
  - [ ] <img src="/img_ico/ebook3000.biz.png"> [ebook3000 - Download PDF Magazines, eBooks, PDF for Free](https://ebook3000.biz/)
  - [ ] <img src="/img_ico/ebook3000.com.png"> [Free eBooks Download - ebook3000.com](http://www.ebook3000.com/)
  - [ ] <img src="/img_ico/thepiratebay.org.png"> [Download music, movies, games, software! The Pirate Bay - The galaxy's most resilient BitTorrent site](https://thepiratebay.org/)
  - [ ] <img src="/img_ico/smallpdf.com.png"> [Smallpdf.com - A Free Solution to all your PDF Problems](https://smallpdf.com/)
  - [ ] <img src="/img_ico/convertcase.net.png"> [Convert Case - Convert upper case to lower case, lower case to upper case and more!](https://convertcase.net/)
  - [ ] <img src="/img_ico/openhardwaremonitor.org.png"> [Open Hardware Monitor - Core temp, fan speed and voltages in a free software gadget](https://openhardwaremonitor.org/)
  - [ ] <img src="/img_ico/tutorialspoint.com.png"> [PowerShell Terminal Online](http://www.tutorialspoint.com/powershell_terminal_online.php)
- [ ] <img src="/img_ico/folder.png"> ###
  - [ ] <img src="/img_ico/real-debrid.com.png"> [Real-Debrid](https://real-debrid.com/downloader)
  - [ ] <img src="/img_ico/alldebrid.com.png"> [AllDebrid: Premium link generator and torrent downloader](https://alldebrid.com/)
- [ ] <img src="/img_ico/folder.png"> Dead
  - [ ] <img src="/img_ico/default.png"> [PCDownload.in](http://pcdownload.in/)
  - [ ] <img src="/img_ico/torrentextra.us.png"> [VuiMedia](http://www.torrentextra.us/tutorial/)
  - [ ] <img src="/img_ico/pdf-giant.com.png"> [PDF Giant](http://pdf-giant.com/english/)

### Learning Certifications (Linux, Cisco, CompTia, CISSP):
- [ ] <img src="/img_ico/folder.png"> Linux
- [ ] <img src="/img_ico/folder.png"> CompTia
- [ ] <img src="/img_ico/folder.png"> CISSP
  - [ ] <img src="/img_ico/isc2.org.png"> [Cybersecurity Certification| CISSP - Certified Information Systems Security Professional | (ISC)�](https://www.isc2.org/Certifications/CISSP)
  - [ ] <img src="/img_ico/google.co.uk.png"> [CISSP: Certified Information Systems Security Professional Official Study Guide - Google Search)](https://www.google.co.uk/search?q=CISSP:+Certified+Information+Systems+Security+Professional+Official+Study+Guide&gws_rd=cr&dcr=0&ei=li-4WuHFGoHNwAKcir_IDw)

### Microsoft:
- [ ] <img src="/img_ico/folder.png"> Learning (Courses / Education / Exams)
  - [ ] <img src="/img_ico/microsoft.com.png"> [Free IT Training, Certification Offers, & Student Discounts | Microsoft (Training Special Offers)](https://www.microsoft.com/en-us/learning/offers.aspx)
  - [ ] <img src="/img_ico/folder.png"> Microsoft Professional Program (Retired 2019/12/31)
    - [ ] <img src="/img_ico/microsoft.com.png"> [Online Data Science Courses | Microsoft Professional Program](https://academy.microsoft.com/en-us/professional-program/tracks/data-science/)
    - [ ] <img src="/img_ico/microsoft.com.png"> [Online Big Data Courses | Microsoft Professional Program](https://academy.microsoft.com/en-us/professional-program/tracks/big-data/)
    - [ ] <img src="/img_ico/microsoft.com.png"> [Online DevOps Courses | Microsoft Professional Program](https://academy.microsoft.com/en-us/professional-program/tracks/devops/)
    - [ ] <img src="/img_ico/microsoft.com.png"> [Online IT Support Courses | Microsoft Professional Program](https://academy.microsoft.com/en-us/professional-program/tracks/it-support/)
    - [ ] <img src="/img_ico/microsoft.com.png"> [Online Artificial Intelligence Courses | Microsoft Professional Program](https://academy.microsoft.com/en-us/professional-program/tracks/artificial-intelligence/)
    - [ ] <img src="/img_ico/microsoft.com.png"> [Online Entry Level Software Development Courses | Microsoft Professional Program](https://academy.microsoft.com/en-us/professional-program/tracks/entry-level-software-development/)
      - [ ] <img src="/img_ico/folder.png"> Expired / Removed Courses
        - [ ] <img src="/img_ico/microsoft.com.png"> [Front-End Web Development (15-30 hours per course)](https://academy.microsoft.com/en-us/professional-program/tracks/front-end-development/)
        - [ ] <img src="/img_ico/microsoft.com.png"> Cloud Administration (12-18 hours per course)
  - [ ] <img src="/img_ico/folder.png"> Replacement Exams to Microsoft Professional Program
    - [ ] <img src="/img_ico/microsoft.com.png"> [Microsoft Certified: Azure AI Engineer Associate](https://docs.microsoft.com/en-us/learn/certifications/azure-ai-engineer)
    - [ ] <img src="/img_ico/microsoft.com.png"> [Microsoft Certified: Azure Data Scientist Associate](https://docs.microsoft.com/en-us/learn/certifications/azure-data-scientist)
    - [ ] <img src="/img_ico/microsoft.com.png"> [Microsoft Certified: Azure Data Engineer Associate](https://docs.microsoft.com/en-us/learn/certifications/azure-data-engineer)
    - [ ] <img src="/img_ico/microsoft.com.png"> [Microsoft Certified: Azure Fundamentals](https://docs.microsoft.com/en-us/learn/certifications/azure-fundamentals)
- [ ] <img src="/img_ico/folder.png"> Windows Devices (Desktop, Laptop, Tablets)
  - [ ] <img src="/img_ico/wikipedia.org.png"> [List of Microsoft Windows versions - Wikipedia](https://en.wikipedia.org/wiki/List_of_Microsoft_Windows_versions)
  - [ ] <img src="/img_ico/folder.png"> 10
    - [ ] <img src="/img_ico/onmsft.com.png"> [Windows 10 build 16299.334 is out is fixes for Bluetooth, PDF rendering and more OnMSFT.com](https://www.onmsft.com/news/windows-10-build-16299-334-is-out-is-fixes-for-bluetooth-pdf-rendering-and-more)
    - [ ] <img src="/img_ico/winaero.com.png"> [Download Windows 10 Build 17127 Official Virtual Machines](https://winaero.com/blog/windows-10-17127-virtual-machines/)
  - [ ] <img src="/img_ico/folder.png"> 8 & 8.1
  - [ ] <img src="/img_ico/folder.png"> 7
  - [ ] <img src="/img_ico/folder.png"> Vista
  - [ ] <img src="/img_ico/folder.png"> XP
- [ ] <img src="/img_ico/folder.png"> Windows Server
  - [ ] <img src="/img_ico/folder.png"> 2019
  - [ ] <img src="/img_ico/folder.png"> 2016
  - [ ] <img src="/img_ico/folder.png"> 2012 & R2
  - [ ] <img src="/img_ico/folder.png"> 2008 & R2
  - [ ] <img src="/img_ico/folder.png"> 2003
  - [ ] <img src="/img_ico/folder.png"> Small Business Server (SBS = Server, Exchange, SharePoint, WSUS)
  - [ ] <img src="/img_ico/folder.png"> Essentials (Replacement for SBS, Limited to 25 Users)
    - [ ] <img src="/img_ico/wikipedia.org.png"> [Windows Server Essentials - Wikipedia](https://en.wikipedia.org/wiki/Windows_Server_Essentials)
    - [ ] <img src="/img_ico/microsoft.com.png"> [Get started with Windows Server Essentials | Microsoft Docs](https://docs.microsoft.com/en-gb/windows-server-essentials/get-started/get-started)
    - [ ] <img src="/img_ico/microsoft.com.png"> [Step 5: Enable folder redirection on the Destination Server for Windows Server Essentials migration | Microsoft Docs](https://docs.microsoft.com/en-us/windows-server-essentials/migrate/step-5--enable-folder-redirection-on-the-destination-server-for-windows-server-essentials-migration)
- [ ] <img src="/img_ico/folder.png"> Exchange
  - [ ] <img src="/img_ico/office.com.png"> [Determine the version of Microsoft Exchange Server my account connects to - Outlook](https://support.office.com/en-us/article/determine-the-version-of-microsoft-exchange-server-my-account-connects-to-d427465a-ce3b-42bd-9d83-c7d893d5d334)
  - [ ] <img src="/img_ico/folder.png"> 2019
  - [ ] <img src="/img_ico/folder.png"> 2016
  - [ ] <img src="/img_ico/folder.png"> 2013
  - [ ] <img src="/img_ico/folder.png"> 2010
  - [ ] <img src="/img_ico/folder.png"> 2007
  - [ ] <img src="/img_ico/folder.png"> 2003
- [ ] <img src="/img_ico/folder.png"> Office 365 / Exchange Online
- [ ] <img src="/img_ico/folder.png"> Sharepoint
- [ ] <img src="/img_ico/folder.png"> Teams / Skype / Lync
- [ ] <img src="/img_ico/folder.png"> Azure
  - [ ] <img src="/img_ico/4sysops.com.png"> [New in Azure networking � 4sysops](https://4sysops.com/archives/new-in-azure-networking/)
- [ ] <img src="/img_ico/folder.png"> Microsoft Office
  - [ ] <img src="/img_ico/folder.png"> 2019
  - [ ] <img src="/img_ico/folder.png"> 2016
  - [ ] <img src="/img_ico/folder.png"> 2013
  - [ ] <img src="/img_ico/folder.png"> 2010
  - [ ] <img src="/img_ico/folder.png"> 2007
  - [ ] <img src="/img_ico/folder.png"> 2003
- [ ] <img src="/img_ico/folder.png"> Microsoft Research
- [ ] <img src="/img_ico/folder.png"> PowerShell




### Telephone Systems (3CX, EVE, Hello, ByPhone):
- [ ] <img src="/img_ico/folder.png"> 3CX


### Web Developement (HTML, CSS, JavaScript, Vue.js, etc):
- [ ] <img src="/img_ico/folder.png"> BootStrap
- [ ] <img src="/img_ico/folder.png"> CSS
- [ ] <img src="/img_ico/folder.png"> Javascript
- [ ] <img src="/img_ico/folder.png"> Vue.js

### GitHub / GitLab:
- [ ] <img src="/img_ico/folder.png"> GitHub
- [ ] <img src="/img_ico/folder.png"> GitLab
  - [ ] <img src="/img_ico/neocities.org.png"> [Neocities: Create your own free website!](https://neocities.org/)

  


### Random:
- <img src="/img_ico/euc365.com.png"> [Getting Useful Device Information… Troubleshooting Made Easy – EUC 365](https://euc365.com/2020/05/23/getdeviceinfo/)
- <img src="/img_ico/microsoft.com.png"> [New & Improved Power Automate Community Cookbook - Power Platform Community](https://powerusers.microsoft.com/t5/News-Announcements/New-amp-Improved-Power-Automate-Community-Cookbook/m-p/440388?amp=1)
- <img src="/img_ico/debian.org.png"> [DebConf20 registration is open! - Bits from Debian](https://bits.debian.org/2020/05/debconf20-open-registration-bursary.html?utm_source=dlvr.it&utm_medium=twitter)
- <img src="/img_ico/youtube.com.png"> [(73) Hayden Barnes: Windows Subsystem for Linux - Interview! - YouTube](https://www.youtube.com/watch?v=OLsGGpxIaUo)
- <img src="/img_ico/youtube.com.png"> [(73) MANGA Storyboards from Script! SECRET TIPS for Making Comics - YouTube](https://www.youtube.com/watch?v=NA2FQ_ZQ7NQ)
- <img src="/img_ico/sporttracks.mobi.png"> [Garmin Varia Radar Data Analysis : Analyze vehicle data with your cycling performance metrics | SportTracks](https://sporttracks.mobi/blog/how-to-analyze-garmin-varia-radar-vehicle-traffic-data)
- <img src="/img_ico/youtube.com.png"> [(73) Node.js Ultimate Beginner’s Guide in 7 Easy Steps - YouTube ](https://www.youtube.com/watch?v=ENrzD9HAZK4)
- <img src="/img_ico/youtube.com.png"> [(73) Learn Git and GitHub Tutorial | Step by Step - YouTube](https://www.youtube.com/watch?v=KA3lFXZD0uw)
- <img src="/img_ico/bleepingcomputer.com.png"> [eBay port scans visitors' computers for remote access programs](https://www.bleepingcomputer.com/news/security/ebay-port-scans-visitors-computers-for-remote-access-programs/)
- <img src="/img_ico/express.co.uk.png"> [Capital cities quiz questions and answers: 15 questions for your home pub quiz | World | News | Express.co.uk](https://www.express.co.uk/news/world/1286357/Capital-cities-questions-answers)
- <img src="/img_ico/doceeo.com.png"> [12 Linux Commands to Have Some Fun in the Terminal](https://www.doceeo.com/post/12-linux-commands-to-have-some-fun-in-the-terminal)
- <img src="/img_ico/youtube.com.png"> [(73) 1 minute PLANK challenge - can you do this? - YouTube](https://www.youtube.com/watch?v=O9NxXkNk-WQ)
- <img src="/img_ico/wellandgood.com.png"> [Here's exactly how to stretch your inner thigh | Well+Good](https://www.wellandgood.com/how-to-stretch-inner-thigh/)



