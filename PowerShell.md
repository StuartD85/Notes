

### PowerShell Tasks:
- [X] Office 365 License Script: To check for unsued licneses
    - [ ] Limit the check to only E1, Business, Essentials, Business Preium
- [ ] Script to Check "Bad Addresses" (which are DEAD) within DHCP
- [ ] When someone leaves or is infected > Run a list of commands
- [ ] Put in place, with the Office 365 commands, to be carried out someone is infected.

The following command, will remove all Active Sessions


<br><br>
### PowerShell Tutorials:
- [ ] SID-500.com - [PowerShell for Beginners (Series)](https://sid-500.com/powershell-for-beginners-series/) Patrick Gruenauer - MVP PowerShell
    - [ ] [(Part 1): The Console and the First Command](https://sid-500.com/2017/12/26/powershell-for-beginners-part-1-the-console-and-the-first-command/)
    - [ ] [(Part 2): The Philosophy Verb-Noun](https://sid-500.com/2017/12/28/powershell-for-beginners-part-2-the-philosophy-verb-noun/)
    - [ ] [(Part 3): The Parameters](https://sid-500.com/2018/01/02/powershell-for-beginners-part-3-the-parameters/)
    - [ ] [(Part 4): The PowerShell Help (Get-Help)](https://sid-500.com/2018/01/06/powershell-for-beginners-part-4-the-help/)
    - [ ] [(Part 5): The Execution Policy (ps1 Files)](https://sid-500.com/2018/01/10/powershell-for-beginners-part-5-the-execution-policy-ps1-files/)
    - [ ] [(Part 6): PowerShell Profiles and the ISE](https://sid-500.com/2018/01/16/powershell-for-beginners-part-6-powershell-profiles-and-the-ise/)
    - [ ] [(Part 7): The Pipe (and many examples to play with)](https://sid-500.com/2018/01/25/powershell-for-beginners-part-7-the-format-commands-and-the-pipe/)
    - [ ] [(Part 8): The Power of PowerShell – Getting in Touch with Objects (Get-Member, Select-Object)](https://sid-500.com/2018/01/28/powershell-for-beginners-part-8-the-power-of-powershell-getting-in-touch-with-objects-get-member-select-object/)
    - [ ] [(Part 9): Get it all from Windows with PowerShell and WMI](https://sid-500.com/2018/02/01/powershell-for-beginners-part-9-get-it-all-from-windows-with-powershell-and-wmi/)
    - [ ] [(Part 10): Filtering and Where-Object](https://sid-500.com/2018/02/15/powershell-for-beginners-part-10-filtering-and-where-object/)
    - [ ] [(Part 11): Having fun with PowerShell Drives](https://sid-500.com/powershell-for-beginners-series/)
    - [ ] [(Part 12): PowerShell Modules](https://sid-500.com/2018/04/10/powershell-for-beginners-part-12-powershell-modules/)
    - [ ] [(Part 13): PowerShell Remoting](https://sid-500.com/powershell-for-beginners-series/)


<br><br>
### PowerShell Code Snippets:
- [Showing the Uptime of all Windows Servers](https://sid-500.com/2018/09/09/powershell-showing-the-uptime-of-all-windows-servers/) Patrick Gruenauer - 9. September 2018 - Ref: [Linkedin](https://www.linkedin.com/feed/update/urn:li:activity:6444464732089909248/)
    <br>*Basics of the Code, but allows for querying all Servers, which could also be done against all PC's within the AD (Active Directory)*
    <br><code>((Get-Date) - (Get-CimInstance -ClassName win32_operatingsystem -ComputerName $PCName -ErrorAction Stop).LastBootUpTime)</code>

- [How to create PowerShell Scheduled Jobs on the Local Computer and on Remote Hosts](https://sid-500.com/2018/08/25/how-to-create-powershell-scheduled-jobs-on-the-local-computer-and-on-remote-hosts/) Patrick Gruenauer - 25. August 2018 - Ref: [Linkedin](https://www.linkedin.com/feed/update/urn:li:activity:6439143841843937280/)
    <br>*Creates a schedule task to do a specific requirment*
    <br>```Register-ScheduledJob -Name InstallXPS -ScriptBlock {Install-WindowsFeature -Name XPS-Viewer} -Trigger (New-JobTrigger -Once -At 05:20pm)```
    <br>```Get-Job```
    <br>```Invoke-Command -ComputerName AzMember01 {Register-ScheduledJob -Name RestartAzMember01 -ScriptBlock {Restart-Computer -Force} -Trigger (New-JobTrigger -Once -At 05:15pm)}```
    <br>```Invoke-Command -ComputerName AzMember01 {Get-Job}```

- [Hyper-V: Backup VMs to a shared folder with Windows Server Backup and a Scheduled Task](https://sid-500.com/2018/08/25/how-to-create-powershell-scheduled-jobs-on-the-local-computer-and-on-remote-hosts/) Patrick Gruenauer - 11. March 2018
    <br>*Note the backup target, which is a shared folder on a NAS and the Hyper-V VMs. The quiet parameter is mandatory, because the backup job will ask if you really want to do this.*
    <br>```wbadmin start backup -backuptarget:\\10.10.0.10\hyper-backup: -hyperv:"Server01,Server02" -allowDeleteOldBackups -quiet```
    <br>```Get-WindowsFeature Windows-Server-Backup```
    <br>```Install-WindowsFeature Windows-Server-Backup -IncludeManagementTools```

- [Test the reachability of all Domain-Controllers: Test-AllDomainController](https://sid-500.com/2018/03/08/test-the-reachability-of-all-domain-controllers-test-alldomaincontroller/) Patrick Gruenauer - 8. March 2018
- [Do-Speak: Start talking with Remote Computers (System.Speech)](https://sid-500.com/2018/03/07/do-speak-start-talking-with-remote-computers-system-speech/) Patrick Gruenauer - 7. March 2018
- [Active Directory Domain Services Section (Version 1.1)](https://sid-500.com/2018/05/22/active-directory-domain-services-section-version-1-1/) Patrick Gruenauer - 22. May 2018
    - [AD Overview Graphical Tool: Active Directory Domain Services Section](https://sid-500.com/2018/03/25/active-directory-domain-services-section-tool-for-active-directory-administrators/) Patrick Gruenauer - 25. March 2018
- [Windows Server: List all installed Roles and Features using PowerShell](https://sid-500.com/2017/07/18/windows-server-list-all-installed-roles-and-features-using-powershell/) Patrick Gruenauer - 18. July 2017
- [How to get a list of all installed Software on Remote Computers](https://sid-500.com/2018/04/02/powershell-how-to-get-a-list-of-all-installed-software-on-remote-computers/) Patrick Gruenauer - 2. April 2018
- [How to schedule software installation with PowerShell](https://sid-500.com/2017/08/02/schedule-the-installation-of-software-with-powershell/) Patrick Gruenauer - 2. August 2017
    <br>`$trigger=New-JobTrigger -Once -At "7/28/2017 10:30 AM"`
    <br>`Register-ScheduledJob -Name WSBackupInstall -ScriptBlock {Install-WindowsFeature -Name Windows-Server-Backup} -Trigger $trigger`
    <br>`Get-Job | Select-Object *`
    <br>`Invoke-Command -ComputerName server02 -ScriptBlock {Register-ScheduledJob -Name WSBackupInstall -ScriptBlock {Install-WindowsFeature -ComputerName server02 -Name Windows-Server-Backup} -Trigger (New-JobTrigger -Once -At "7/28/2017 10:30 AM")}`
    <br>`Invoke-Command -ComputerName server02 {Get-Job | Select-Object PSComputerName,PSBeginTime,PSEndtime; Get-WindowsFeature Windows-Server-Backup | Select-Object Name,PSComputerName,Installstate}*`
- [How to automatically restart applications when they were closed](https://sid-500.com/2017/09/16/powershell-how-to-automatically-restart-applications-when-they-are-closed/) Patrick Gruenauer - 16. September 2017
- [Configuring the automatic start of PowerShell at every logon](https://sid-500.com/2017/07/26/how-to-automatically-start-powershell-at-every-logon/) Patrick Gruenauer - 26. July 2017
- [Monitoring Windows PowerShell: Enable Module Logging](https://sid-500.com/2017/08/16/monitoring-windows-powershell-enable-module-logging/) Patrick Gruenauer - 16. August 2017
- [Getting Windows Defender Status from all Domain Joined Computers (Get-AntiMalwareStatus)](https://sid-500.com/2018/08/27/powershell-getting-windows-defender-status-from-all-domain-joined-computers-get-antimalwarestatus/) Patrick Gruenauer - 27. August 2018



<br><br>
---
### PowerShell Notes:
**When someone leaves or is infected > Run a list of commands:**

Put in place, with the Office 365 commands, to be carried out someone is infected.

The following command, will remove all Active Sessions

<code>Connect-AzureAD</code><br>
<code>Get-AzureADUser -Searchstring user@company.com | Revoke-AzureADUserAllRefreshToken</code>

---

**Examples:**
* [x] Done
* [ ] To be Done
* [>] Scheduled
* [-] Cancelled

---
### Office 365 License Script: To check for unsued licneses





























