# Notes

Different Markdown Notes for Ongoing or Current Projects

List of Notes:
- [ ] Vue.js Cookbook / Vuetify
- [ ] LinkedIn - [The secret to getting everyday IT projects completed on time and within budget](https://www.linkedin.com/pulse/secret-getting-everyday-projects-completed-time-within-keith-edmunds/) Keith Edmunds - September 4, 2018


Group Policy / Registry:
Outlook:
- [ ] Disable Offline Mode

Windows 10:
- [ ] Disable Quick Boot: From within the Windows 10 Power Options
- [ ] Set Updates after 6pm, New control Panel
- [ ] Tablet Mode Disabled, unless x32 OS
- [ ] Disable Flight Mode, unless x32 OS
- [ ] Enable Storage Sense, specially on Tablets x32 OS
- [ ] Script to do Weekly Clean-Up if unable to do above
- [ ] Setting Default Mail App to Outlook (New Control Panel)

All Windows:
- [ ] Disable Windows / Outlook Login?

Server:
- [ ] Dave A's .ps1 Temp File Cleaner (Weekly)



PowerShell:
*Moved to PowerShell.md*



AbiliyIT:Hub
- [ ] Ini File Config
- [ ] PC Info / Diagnostics / Advanced / Capture
- [ ] Diagnostics:
Query:
- [ ] SendGrid: SMTP



3CX:
- Log into Queue: *62
- Log out of Queue: *63
- Voice Mail: #200 pin e.g. 6918#

AV (Anti-Virus)
- [ ] When Scanning Hyper-V folder do an exclusion, and an exclusion on the custom folders
- [ ] SendGrid = SMTP

Local PC Accounts:
*Local User Accounts do have "Logon Script, User Profile, and Home Directory" showing when doing Net User.*
- Set a Logon Script to do Mapped Drives on a Local Profile
- Also set to start LogMeIn
- If a User has OneDrive as a working Function. Set it to Start OneDrive to start up the Sync...


**Examples:**
* [x] Done
* [ ] To be Done
* [>] Scheduled
* [-] Cancelled



