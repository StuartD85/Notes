

# Linux Foundation Certified SysAdmin - LFCS:
Linux Foundation Certified System Administrator - LFCSA<br>
*The exam is currently on version 3.18? - Linux Academy released a notification on a refresh of the exam on the 29/05/2018. With the syllabus changes on 12th June.*<br>
- [ ] [Microsoft Linux on Azure (MCSA)](https://training.linuxfoundation.org/certification/microsoft-linux-on-azure-mcsa/?_sft_category=certification) £300
- [ ] [Linux Foundation Certified SysAdmin (LFCS)](https://training.linuxfoundation.org/certification/linux-foundation-certified-sysadmin-lfcs/) £300
- [ ] [Linux Foundation Certified Engineer (LFCE)](https://training.linuxfoundation.org/certification/linux-foundation-certified-engineer-lfce/) £300
- [GitHub - Kickball / awesome-selfhosted](https://github.com/Kickball/awesome-selfhosted/#bookmarks-and-link-sharing) *Reference Link*

##### LFCS: References
#### LiNUX: References
Linux Foundation - Training:


### LFCS: Tutorial's
- [ ] [LiveLessons (Pearson) - Beginning Linux System Administration](https://www.sandervanvugt.com/course/beginning-linux-system-administration-livelessons/) May 10, 2016
- [ ] [CompTIA Linux+ / LPIC-1](https://www.sandervanvugt.com/course/comptia-linux-lpic-1-complete-video-course/)
- [ ] [RHCSA with Virtual Machines - 2nd Edition](https://www.sandervanvugt.com/course/red-hat-certified-systems-administrator-complete-video-course/)
- [ ] [Red Hat Certified System Administrator Exam Prep Video Workshop](https://www.sandervanvugt.com/course/red-hat-certified-system-administrator-exam-prep-video-workshop-download/)
- [ ] [LiveLessons (Pearson) - Linux Foundation Certified System Administrator (LFCS) - Sander van Vugt](https://www.sandervanvugt.com/course/linux-foundation-certified-system-administrator-complete-video-course/) Jun 12, 2017
    - [ ] [Lesson 5: Connecting to a Server](https://www.youtube.com/watch?v=Otno7HCJqss) Sander van Vugt, RhatCertification - 16 Apr 2018
- [ ] [Linux Training Academy (Udemy) - Linux System Administration Crash Course](https://www.youtube.com/watch?v=qAMWG86sEm8&t=2s) Jason Cannon - 12 Jul 2015
    - [ ] [Linux tar Command Tutorial](https://www.linuxtrainingacademy.com/tar/)
    - [ ] [Linux Commands Cheat Sheet](https://www.linuxtrainingacademy.com/linux-commands-cheat-sheet/)
    - [ ] [Vim Cheat Sheet](https://www.linuxtrainingacademy.com/vim-cheat-sheet/)
    - [ ] [Over 1,500 Coding Project Ideas](https://www.linuxtrainingacademy.com/projects/)
- [ ] [Pluralsight - Linux Foundation Certified System Administrator (LFCS)](https://www.pluralsight.com/paths/linux-foundation-certified-system-admin) Andrew Mallett - 2016
    - [ ] Learning the Essentials of CentOS Enterprise Linux 7 Administration - Jan 19, 2016 / 6h 43m
    - [ ] CentOS Enterprise Linux 7 Operation Essentials - Feb 4, 2016 / 6h 50m
    - [ ] CentOS Enterprise Linux 7 User and Group Management - Mar 1, 2016 / 4h 20m
    - [ ] CentOS Enterprise Linux 7 Storage Management - Mar 24, 2016 / 6h 23m
    - [ ] CentOS Enterprise Linux 7 Network Management - Apr 20, 2016 / 4h 7m
    - [ ] LFCS: Linux Service Management - May 26, 2016 / 7h 2m
    - [ ] LFCS: Linux Virtualization Management - Jun 15, 2016 / 3h 21m
- [ ] [The Urban Penguin](https://www.youtube.com/user/theurbanpenguin) Andrew Mallett - n/a
- [ ] [Linux Academy - (LFCS)](https://linuxacademy.com/linux/training/course/name/linux-foundation-certified-system-administrator-v3-18): v3.18
    - [ ] [Intro to: Linux Foundation Certified System Administrator (v3.18)](https://www.youtube.com/watch?v=yw0LwtU0eCA)
        - *That is not up to date. No service configuration is currently included into the exam.* Igal Milchakov - October
- [ ] [Lynda - Linux](https://www.lynda.com/Linux-training-tutorials/1277-0.html)
- [ ] [TecMint Ebook: Introducing the Linux Foundation’s LFCS and LFCE Certification Preparation Guide](https://www.tecmint.com/linux-foundation-lfcs-lfce-certification-exam-book/) Updated August 13, 2018
- [ ] [TecMint - LFCS (Linux Foundation Certified Sysadmin)](https://www.tecmint.com/sed-command-to-create-edit-and-manipulate-files-in-linux/) *Main page with sub Links*
    - [ ] [TecMint - LFCS Category)](https://www.tecmint.com/category/lfcs/)
- [ ] CertDepot - [Everything you need to pass your RHCSA, RHCE, LFCS, LFCE and much more](https://www.certdepot.net/)
    - [ ] [RHEL7 (LFCS) Quick recipes](https://www.certdepot.net/rhel7-quick-recipes/)

##### CompTIA - Linux+ (Powered by LPI)





### LFCS: Reference Material
- [ ] [Linux Foundation - CERTIFIED SYSTEM ADMINISTRATOR (LFCS) EXAM](https://community.spiceworks.com/topic/1472201-linux-foundation-certified-system-administrator-lfcs-exam) Neally on Mar 1, 2016 at 8:21 PM
    - [ ] [SAM: Learning Linux System Administration](https://mangolassi.it/topic/7825/sam-learning-linux-system-administration) scottalanmiller - 1 Feb 2016, 09:20 
    - [ ] [Ryans Tutorials - Linux](https://ryanstutorials.net/linuxtutorial/) n/a - n/a 
    - [ ] *Recommended: Pluralsight - LFCS - Andrew Mallet*
    - [ ] *Recommended: Linux Academy*
    - [ ] [The Urban Penguin](https://www.youtube.com/user/theurbanpenguin) Andrew Mallett - n/a
- [ ] [StackExchange - Unix - LFCS exam vs. LFS201 training topics](https://unix.stackexchange.com/questions/357940/lfcs-exam-vs-lfs201-training-topics) Ariel - Apr 9 '17 at 13:24
    - [ ] [Edx.org - Linux Foundation Training Course LFCS-101x](https://www.edx.org/course/introduction-linux-linuxfoundationx-lfs101x-1) n/a - n/a 
    - [ ] *Recommended: Linux Foundation Training Course LFS201*
    - [ ] [Channel9 - MS Ignite 2016 - Cert Exam Prep: Linux Foundation Certified System Administrator (LFCS)](https://channel9.msdn.com/Events/Ignite/2016/BRK3268) Mark Grimes - Oct 06, 2016 2:00PM
    - [ ] *Recommended: Linux Fundamentals – Paul Cobbaut (Downloaded PDF)*
    - [ ] *Recommended: Pluralsight - LFCS - Andrew Mallet*
    - [ ] *Recommended: Visual Studio - Dev Essentials Training Course Voucher for Pluralsight, Linux Academy*
    - [ ] [Blue Clouds - Linux Foundation Certified System Administrator  – Exam Experience & Tips](https://www.edx.org/course/introduction-linux-linuxfoundationx-lfs101x-1) Chris Becckett - 16 Dec 2016
- [ ] [My feedback about the LFCS certification](https://en.blog.madrzejewski.com/feedback-lfcs-certification-linux-foundation/) Alexis Madrzejewski - 15 September 2016
- [ ] [vMusketeers - (LFCS) Exam Review](http://vmusketeers.com/2017/12/18/linux-foundation-certified-systems-administrator-lfcs-exam-review-linuxfoundation-linuxfoundation-linux-centos-sandervanvugt-tecmint-vexpert/) Bilal Ahmed - December 18, 2017 
    - [ ] [Techmint](https://www.tecmint.com/linux-foundation-lfcs-lfce-certification-exam-book/) *Recmmends the Site Tutorial and the PDF*
    - [ ] *Recommended: LiveLessons (Pearson) - (LFCS) by Sander van Vugt]*
- [ ] [vMusketeers - (LFCE) Exam Review](http://vmusketeers.com/2018/01/16/linux-foundation-certified-engineer-lfce-exam-review-linuxfoundation-linuxfoundation-linux-centos-sandervanvugt-tecmint-vexpert/) Bilal Ahmed - January 16, 2018 
    - [ ] *Recommended: LiveLessons (Pearson) - (LFCE) by Sander van Vugt]*
    - [ ] *Recommended: TecMint Exam PDF*
- [ ] [Review of LFCS Ubuntu exam)](https://eatpeppershothot.blogspot.com/2015/03/review-of-lfcs-ubuntu-exam-taken-on-feb.html) Feb. 28 2015
- [ ] [Reddit (r/linuxadmin) - LFCS - Am I ready?](https://www.reddit.com/r/linuxadmin/comments/95vye5/lfcs_am_i_ready/) carl_roberts - Aug 2018
    - *Check at the bottom of the page for more information* 
- [Linux Command Line Cheat Sheet](https://www.cheatography.com/davechild/cheat-sheets/linux-command-line/) DaveChild - 2011
- [ExplainShell.com](https://explainshell.com/) *write down a command-line to see the help text that matches each argument*

<br><br>
### Linux: Reference Material
- [ ] [Linux Screw](http://www.linuxscrew.com/)
- [ ] [Lynda - Linux](https://www.lynda.com/Linux-training-tutorials/1277-0.html)
- [ ] [nixCraft](https://twitter.com/nixcraft)
- [ ] [Awosome-Tech](https://awesome-tech.readthedocs.io/linux/#sysadmin-blogs) *Not so sure on the content?*

<br><br>
##### TecMint
- [ ] [BEGINNER’S GUIDE FOR LINUX – Start Learning Linux in Minutes](https://www.tecmint.com/free-online-linux-learning-guide-for-beginners/) Updated August 9, 2017 
- [ ] [10 Useful Free Linux eBooks for Newbies and Administrators](https://www.tecmint.com/10-useful-free-linux-ebooks-for-newbies-and-administrators/) Updated September 15, 2015
- [ ] [Linux Tricks](https://www.tecmint.com/tag/linux-tricks/) *Multiple sub links*
- [ ] [Linux Commands](https://www.tecmint.com/category/linux-commands/) *Multiple sub links*
- [ ] [Top Tools](https://www.tecmint.com/category/top-tools/) *Multiple sub links*
- [ ] [Perf- A Performance Monitoring and Analysis Tool for Linux](https://www.tecmint.com/perf-performance-monitoring-and-analysis-tool-for-linux/) Updated May 21, 2016
- [ ] [How to Add Linux Host to Nagios Monitoring Server Using NRPE Plugin](https://www.tecmint.com/how-to-add-linux-host-to-nagios-monitoring-server/) Updated August 8, 2018
- [ ] [Install Mtop (MySQL Database Server Monitoring) in RHEL/CentOS 6/5/4, Fedora 17-12](https://www.tecmint.com/install-mtop-mysql-database-server-monitoring-in-rhel-centos-6-5-4-fedora-17-12/) Updated May 9, 2013
- [ ] [10 Wget (Linux File Downloader) Command Examples in Linux](https://www.tecmint.com/10-wget-command-examples-in-linux/) Updated January 3, 2015
- [ ] [Netdata – A Real-Time Performance Monitoring Tool for Linux Systems](https://www.tecmint.com/netdata-real-time-linux-performance-network-monitoring-tool/) Updated March 29, 2018
- [ ] [11 Cron Scheduling Task Examples in Linux](https://www.tecmint.com/11-cron-scheduling-task-examples-in-linux/) Updated May 25, 2015
- [ ] [How to Install and Configure ‘Collectd’ and ‘Collectd-Web’ to Monitor Server Resources in Linux](https://www.tecmint.com/install-collectd-and-collectd-web-to-monitor-server-resources-in-linux/) Updated June 30, 2015
- [ ] [How to Sync Two Apache Web Servers/Websites Using Rsync](https://www.tecmint.com/sync-two-apache-websites-using-rsync/) Updated January 7, 2015
- [ ] [How to Install Nagios 4.3.4 on RHEL, CentOS and Fedora](https://www.tecmint.com/install-nagios-in-linux/) Updated October 26, 2017
- [ ] [Install Munin (Network Monitoring) in RHEL, CentOS and Fedora](https://www.tecmint.com/install-munin-network-monitoring-in-rhel-centos-fedora/) Updated June 30, 2017
- [ ] [Understand Linux Load Averages and Monitor Performance of Linux](https://www.tecmint.com/understand-linux-load-averages-and-monitor-performance/) Updated May 31, 2017
- [ ] [Install Cacti (Network Monitoring) on RHEL/CentOS 7.x/6.x/5.x and Fedora 24-12](https://www.tecmint.com/install-cacti-network-monitoring-on-rhel-centos-6-3-5-8-and-fedora-17-12/) Updated January 27, 2017
- [ ] [20 Command Line Tools to Monitor Linux Performance](https://www.tecmint.com/command-line-tools-to-monitor-linux-performance/) Updated January 3, 2015





<br><br><br><br><br><br><br><br><br><br><br><br>
### LFCS: Test References
- [ ] [Practice test!](http://mathematicbren.blogspot.com/2014/12/practice-test.html) Brendan Swigart - Tuesday, December 9, 2014
- [ ] CertDepot - [Everything you need to pass your RHCSA, RHCE, LFCS, LFCE and much more](https://www.certdepot.net/)


<br><br><br><br>
### LFCS: People's Reviews / Opinions
- [ ] [Reddit (r/linuxadmin) - LFCS - Am I ready?](https://www.reddit.com/r/linuxadmin/comments/95vye5/lfcs_am_i_ready/) carl_roberts - Aug 2018
    - [GitHub - Kickball / awesome-selfhosted](https://github.com/Kickball/awesome-selfhosted/#bookmarks-and-link-sharing) *Reference Link*
    - [TecMint LFCS](https://www.reddit.com/r/linuxadmin/comments/95vye5/lfcs_am_i_ready/) *Reference Link*
    - Install LXC/LXD, Docker, [mentions the pratice test example above](http://mathematicbren.blogspot.com/2014/12/practice-test.html?m=1), [working through complex sed substitutions](https://www.tecmint.com/sed-command-to-create-edit-and-manipulate-files-in-linux/), 

*I do think it is a good test (as good as RHCSA), but you really need to prep well for it. Good thing it allows retakes :)*

*The Linux Foundation course at that time seemed comprehensive, but I'd say 50% it is not even on the exam. Its just a good overview of Linux in general. The Linux Academy course was horrificly inadequate. They have however updated to a new version (3.18)*

*I highly recommend getting your hands dirty and practicing everything over and over until it is second nature. My tips are make sure you can perform everything on the test on a VM instance very quickly from memory. Get very good, fast at file manipulation, searching, replacing etc (using sed, vi, diff, sort etc). There are a lot of exercises on this. I had LVM + encrypted volumes - make sure you are fast at this as it eats up a lot of time on the test if you get exercises with it. There were a good portion of stuff on user mgmt but IMO that was pretty straightforward (groups, sudo setup). I also had very detailed KVM stuff which blindsided me since it wasnt covered at all in that detail in either training course. ( I do think they have since removed this from the exam requirements, so that is good.) You could also be asked to configure bind, postfix, apache, proxy, from scratch. Although in my exam I was not...*

*Sounds like a good plan! I looked over that practice test that moofishies posted and looks good generally (the file manipulation, user management especially). It IS from 2014 and the test was waaaay easier back then compared to now. I'd double check the test areas via this link to be sure: https://training.linuxfoundation.org/certification/linux-foundation-certified-sysadmin-lfcs/#exams*

*Dont forget to practice quotas, apache, postfix, dovecot (imap/s), mysql, bind, SELinux/AppArmor (depending on which distro you chose), PAM modules (I forgot I DID have this and it was not easy :( ) ...looks like they added containers too. not sure what that is about I assume using docker? Its not super clear :( If you can get Linux Academy on a trial basis it might be worth running through their course.*

<br><br>
- TechExams.net - [LFCS on 10/19/17](http://www.techexams.net/forums/lpi-rhce-sair/129640-lfcs-10-19-17-a.html)
    - [Cryptography, Linux, and me: Practice test!](http://www.techexams.net/forums/lpi-rhce-sair/129640-lfcs-10-19-17-a.html)
    - CertDepot - [Everything you need to pass your RHCSA, RHCE, LFCS, LFCE and much more](https://www.certdepot.net/)

*First with the LFS101 on Edx and then with Sander Van Vugt's LFCS videos on Safari Books.*<br><br>
*I tried the official LFS201 and find the material to be very college lecture-like and only vaguely covering what ought to be on the exam. Even coop, who seems to be the course author admits that the course designers and the exam designers don't coordinate with each other when they create. WTF?*<br><br>
*Auto tab completion was a little weird. It worked most of the time but on occasion double-tabbing to see a list didn't work.*<br><br>
*Here's a tip: I used vi for text editing. Step one, create a file in home called .vimrc. Add the single line: set number*<br>
*Save it. This will add line numbering by default. If your keyboard arrow keys dont' move the cursor but instead print out BA, etc. add the additional line:
set nocompatible*<br>
*Using vi without arrow keys is a little too retro for my taste.*<br><br>
*I mostly did the questions in sequence so I didn't have to deal with remembering which ones I completed. Maybe not the most ideal strategy, since some questions took 15+ minutes each to do and others literally less than 30 seconds to do. I didn't see point weights listed anywhere though I've heard not all points are equal.*<br><br>
*That blog list looks pretty accurate **"Cryptography, Linux, and me: Practice test!"**, though there were other things. I have a suspicion that the question bank for the LFCS sticks to relatively distro-neutral stuff, so probably not going to get hardcore SELinux or AppArmor questions. Or probably not deep yum/apt questions as they also differ.*<br><br>
*I learned how to use TMUX and am glad I did, as it is super helpful to be able to split the terminal window in two, having a man page open as a reference to get the syntax down for a more complex command. I've seen it blogged not to waste time doing this. I disagree.*<br><br>
*Linux Foundation says that Ctrl+c will break the exam, so I got in the habit of instead using Ctrl-d instead. Not ideal, but at least something. I've also read that using nano might mess up the exam because of the needed Ctrl-x and Ctrl-o commands. While I used vi and not nano, I did use visudo for something. visudo seems to be based on nano and I had no problems saving my changes. I used the Chrome browser. Perhaps the person having trouble was on FireFox or something. *<br><br>
*Many of the questions were repeats, but some were different. There was one question that I didn't know dealing with a legacy boot parameter setting. I also brainfogged and started setting up LVM partitioning wrong. I ended up running partprobe and the OS complained about needing a reboot. Never encountered that before.*<br><br>
*It took about 5 minutes to reboot, my mind cleared while waiting, and figured out the LVM setup. But I think the reboot messed something up with the network share. Was getting a "client mount RPC error: program not registered." I probably should have raised the issue but I didn't and let it go. So I know it wasn't a perfect score. *

<br><br>
**(LFCS) Exam Review**<br>
- [LFCS V2.16 vs 3.18 - line by line comparison](https://linuxacademy.com/community/posts/show/topic/25261-lfcs-v216-vs-318-line-by-line-comparison) Kastus K - Mar 05, 2018
<br><br>
*Bilal Ahmed - December 18, 2017*<br>
*I created myself two Centos 7 VMs and gave them lots of disks and took a snapshot and then went through the course. I broke the VMs multiple times and reverted the snapshots etc.*<br><br>
**Tips:**
- Know user manipulation very very well, everything to do with users/groups etc know it like the back of your hand
- Know disk manipulation, partitioning,resizing, mounting
- Know how to use text editors well
- Know how to search/find/copy files and inside files too
- Know permission structures and changing them as required

<br><br>
**(LFCE) Exam Review**<br>
*Bilal Ahmed - January 16, 2018*<br>
Now the blueprint for the LFCS was very very broad, to the point it made it very difficult to study for, but the blueprint for the LFCE is much more concise<br>

A lot of the things I covered in my LFCS post are totally valid in the LFCE too. I never needed to reboot the VM, since it is CentOS 7 you can pick to do things in different ways for example you can use IPTABLES or Firewalld, the choice is yours, as long as you get the correct end result. This is the same for a lot of admin/engineer lab exams, there is always more than one way to do things, but the key is the end result. Anyone who has written the VMware VCAP Deploy exams will know what I mean.<br>
*I used the same CentOS7 VMs I had created for the LFCS exam, and just kept going through the labs in the video course and just messing around, until I felt I could do most stuff without referring to anything, or I knew where to look, be it the man pages or in /usr/share/doc/ as that is all you will have access too! You should know firewalls and SELinux inside and out, it’s taught very well in the video course.*

*Services/firewalls/SElinux/web services/email all those kind of things. But of course, you have to know your permissions, creating users etc etc*
*As many people have mentioned on reddit etc, the LFCS is heavily focused on users more than anything else. The LFCE focuses much more on functionality, services, security etc*

<br><br>
**Linux Academy - Community:**<br>
[LFCSA v2.16](https://linuxacademy.com/community/posts/show/topic/27309-linux-foundation-certified-systems-engineer-v318):<br>
*'Create the NFS common file in the '/etc/default' directory. Add the setting that indicates that IDMAPD is needed for NFS file sharing. Edit the IDMAP configuration file, add the domain of the server (provided on the lab server page)'*<br>
The course has two videos on setting up NFS shares in the Service Configuration section, and neither video makes reference to creating an NFS common file, or IDMAPD

<br><br>
[LFCS Practice Exam, Questions, or Labs?](https://linuxacademy.com/community/posts/show/topic/24437-lfcs-practice-exam-questions-or-labs):<br>
*As a result, if you like, you can leverage the quizzes, exercises and labs in Red Hat (RHCSA) and LPI (LPIC-1 103/104) to give you additional information and practice to prepare for LFCS.*
*JUST this week, we added a full exam walk through Learning Activity (hands on) to the Red Hat Certified System Administrator course. The exams for RHCSA and LFCS are VERY similar, if you would like even more practice, try that one.*

<br><br>
[Passed the LFCS!](https://linuxacademy.com/community/posts/show/topic/5593-passed-the-lfcs):<br>
*I started out with going through the LFS-101 and LFS-201 courses via linuxfoundation.org. The amount of information there was enough to fill a few notebooks and provide some excellent labs. With that said, I was a bit disappointed with some of the obvious errors in the lab work there. Along with that, I heavily relied on the training here, but as I am a 'hands on' learner and had walked through the LFCS labs here to the point of memorization, I did a few things:*
1) I found this and the other 9 sections very helpful: http://www.tecmint.com/sed-command-to-create-edit-and-manipulate-files-in-linux/<br>
2) I used this guys labs as supplementation to the information here:
http://leonelatencio.com/wp-content/uploads/2014/11/Linux-Foundation-Certified-System-Administrator-LFCS-v1.3.pdf<br>
3) I wrote my own practice tests and then made multiple iterations of the test. This forced me to not only know how to setup and complete the various scenarios, but how to tear them down again. Sure, you might be able to create a few Logical Volumes, but can you safely tear them down again in the right order? How about build new ones on the same devices? Memorize it and everything else forwards and backwards and you'll be guaranteed to know and understand the whole process. The biggest thing is to practice and practice a lot.<br>
4) The other areas of this site. I spent a lot of time at the Linux Academy watching videos and doing labs from other areas outside of the LFCS path. For example, check out the CIFS and NFS Shares on CentOS 7 and the various 'Find' command labs. Review the Linux Essentials course as well as some of the videos and labs in the LPIC section.<br>
Finally, remember to remain calm and cool headed during the exam. The two hours moves by quickly, but if you pace yourself and rely on your training and intuition, you'll do just fine! Best of luck!!

<br><br>
**LFCS exam vs. LFS201 training topics**<br>
*vitaminace33 - Apr 25 at 4:49:*<br>
On the one hand, I consider myself an advanced user: I admin several machines (some of my own, others not), which I have all installed and where I set up different services (ssh, http, vpn, (s)ftp, samba, etc.). I like to write my own bash scripts. I am not a sysadmin pro, since it is not my job, but one could say I am a sysadmin amateur (an advanced one).

The exam consists of 30 questions, each one of them with 3 to 5 literals (so to say). Sometimes the literals must me achieved sequentially, sometimes not. Some of the literals may require several actions, some simply one. Some questions/literals are not easily understandable. You have less than 150 minutes to complete the exam, i.e. 5 minutes per question. Taking into account that reading and understanding each question will take you at least 2 minutes, you are left with 3 minutes per question to answer them. If you use man pages, which you can, you will lose lots of time.

In terms of content, the exam goes deep into the LFS201 course contents and far beyond. Different subjects intertwine at each question, for instance you might be asked to turn on a virtual machine, make sure that it starts at boot and that it uses a file which is linked to another file and that their SELinux/AppArmor context are consistent. You might be asked to use a PAM module, which you will have to determine, in order to achieve a specific goal. You must know the basics of bash scripting and the use of common shell tools like grep, sed, find and, obviously, all the basics ls, cp, mv, rm, etc., but at a really advanced level (know most of the options of each). You must know how to install and configure to some extent well known services ssh, http, etc. Also, the order of the questions guards no relation with the order of the subjects in the course, you might be asked about security, then user management and then system rescue.

There is a big gap between the LFS201 course and the LFCS exam. I can easily do any course lab (and much more) with no help, but I had to look at the man pages at each and every exam question (even for a simple cp), which was a great loss of time and it did not always gave the answer I sought (master the use of man!). Moreover, some (several) questions are not covered in the course or, at least, not in the same detail. For instance, the LFS201 course explicitly avoids iptables, which was the 2nd question on my exam. I achieved a 40% of the exam, but I think I could have completed a 90% of it with the twice/thrice the time.

In summary, my opinion/complain: I do like the exam contents, it is complete and non-trivial, which gives reputation to the certification. So I am happy with that. However, I truly believe that (1) the environment is counter-productive (they should look for a solution on the shortcuts/hotkeys), (2) the time is not sufficient and, most importantly, (3) there should be labs at the same level than the exam. I have payed for a course which is supposed to be a preparation to an exam, but it is not, it is merely an extended introduction. It pisses me off to think I have the ability to complete the exam but I couldn't because I was not well advised.

Anyhow, I will give it another try, because I am confident I can do it. If you plan to take the exam, my advises: (1) master man-pages, (2) know the most part of the options of "basic" commands, (3) learn the basic of common services, (4) go deep into all the course's chapters and (5) try to think in intertwined scenarios.

<br><br>
Karl.Orange - [Tips on the Linux Foundation Certified SysAdmin Exam](https://karlorange.wordpress.com/2015/03/19/tips-on-the-linux-foundation-certified-sysadmin-exam/) Carlos J. Naranjo - March 19, 2015<br>
    - [Tips for RHCSA and RHCE exams](https://karlorange.wordpress.com/2015/02/21/tips-for-rhcsa-and-rhce-exams/) Carlos J. Naranjo - February 21, 2015
    - [Essentials of System Administration (LFS201)](http://training.linuxfoundation.org/essentials-of-system-administration)
    - [TecMint’s 10 Lessons to Prep for LFCS exam](http://www.linux.com/learn/linux-certifications/795683-tecmints-10-part-guide-to-linux-foundation-sysadmin-certification)
    - [Review of LFCS Ubuntu exam](http://eatpeppershothot.blogspot.kr/2015/03/review-of-lfcs-ubuntu-exam-taken-on-feb.html) – a different blog showing someone else’s experience

*As explained earlier you do not have a graphic environment nor do you have access to a system console, so when performing sudoers, fstab, rc script modifications and such, make sure you validate the modifications thoroughly (syntax, format, permissions, etc) to avoid breaking something.*











